package ua.greencampus.service;

/**
 * @author Nikolay Yashchenko
 */
public interface AuthenticationService {
    Long getLoggedInUserId();
}
